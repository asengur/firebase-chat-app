//
//  UsersTableViewCell.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 21.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class UsersTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var selectedUser: User!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func setTableViewCell(user: User) {
        selectedUser = user
        usernameLabel.text = user.username
        emailLabel.text = user.email
    }
    

}
