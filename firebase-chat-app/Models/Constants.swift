//
//  Constants.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 21.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation


let USERS_REF = "Users"
let USERNAME = "Username"
let EMAİL = "Email"
let USER_CREATION_DATE = "CreationDate"
let SENDER_UID = "SenderUID"
let RECEIVER_UID = "ReceiverUID"
let MESSAGE_TEXT = "MessageText"
let MESSAGE_CREATION_DATE = "MessageCreationDate"
