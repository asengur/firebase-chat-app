//
//  User.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 21.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Firebase


class User {
    
    private(set) var email: String!
    private(set) var username: String!
    
    init(email: String, username: String) {
        self.email = email
        self.username = username
    }
    
    
    class func getUser(snapshot: QuerySnapshot?) -> [User] {
        var users = [User]()
        
        guard let snap = snapshot else { return users }
        
        for record in snap.documents {
            let data = record.data()
            let username = data[USERNAME] as? String ?? "Not User"
            let email = data[EMAİL] as? String ?? "Not User Email"
            let newUser = User(email: email, username: username)
            users.append(newUser)
        }
        return users
    }
    
}
