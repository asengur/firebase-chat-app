//
//  Message.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 21.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Firebase


class Message {
    
    private(set) var senderUID: String!
    private(set) var messageText: String!
    private(set) var creationDate: Date!
    private(set) var receiverUID: String!
    private(set) var documentID: String!


    init(senderUID: String, messageText: String, creationDate: Date, receiverUID: String, documentID: String) {
        self.senderUID = senderUID
        self.messageText = messageText
        self.creationDate = creationDate
        self.receiverUID = receiverUID
        self.documentID = documentID
    }
    
    
    class func getMessage(snapshot: QuerySnapshot?) -> [Message] {
        var messages = [Message]()
        guard let snap = snapshot else { return messages }
        for document in snap.documents {
            let data = document.data()
            
            let senderUID = data[SENDER_UID] as? String ?? "sender_uid"
            let messageText = data[MESSAGE_TEXT] as? String ?? ""
            let ts = data[MESSAGE_CREATION_DATE] as? Timestamp ?? Timestamp()
            let creationDate = ts.dateValue()
            let receiverUID = data[RECEIVER_UID] as? String ?? "receiver_uid"
            let documentId = document.documentID
            
            let newMessage = Message(senderUID: senderUID, messageText: messageText, creationDate: creationDate, receiverUID: receiverUID, documentID: documentId)
            messages.append(newMessage)
        }
        return messages
    }
    
}




