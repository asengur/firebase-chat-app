//
//  NewConversationViewController.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore



class NewConversationViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var users = [User]()
    private var usersCollectionRef: CollectionReference!
    private var usersListener: ListenerRegistration!
    private var listenerHandle: AuthStateDidChangeListenerHandle?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        usersCollectionRef = Firestore.firestore().collection(USERS_REF)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setListener()
    }
    
    
    func setListener() {
        
        usersListener = usersCollectionRef.addSnapshotListener({ (snapshot, error) in
            if let error = error {
                debugPrint("Get document error: \(error.localizedDescription)")
            } else {
                self.users.removeAll()
                self.users = User.getUser(snapshot: snapshot)
                self.tableView.reloadData()
            }
        })
        
    }
}



extension NewConversationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UsersTableViewCell {
            cell.setTableViewCell(user: users[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let sendMessageVC = storyboard.instantiateViewController(identifier: "SendMessageViewController") as? SendMessageViewController {
            sendMessageVC.titleText = users[indexPath.row].username
            self.navigationController?.pushViewController(sendMessageVC, animated: true)
        }
    }
    
    
}
