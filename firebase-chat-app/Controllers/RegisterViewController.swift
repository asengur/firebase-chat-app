//
//  RegisterViewController.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseFirestore

class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordtextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signUpButton.layer.cornerRadius = 6
        backButton.layer.cornerRadius = 6
    }
    
    @IBAction func didTapSignUpButton(_ sender: UIButton) {
        
        guard let email = emailTextField.text,
        let password = passwordtextField.text,
            let username = usernameTextField.text
        else { return }
        
        Auth.auth().createUser(withEmail: email, password: password){(authResult, error) in
            if let error = error {
                debugPrint("Sign up error : \(error.localizedDescription)")
            }
            
            let changeRequest = authResult?.user.createProfileChangeRequest()
            changeRequest?.displayName = username
            changeRequest?.commitChanges(completion: {(error) in
                if let error = error {
                    debugPrint("Error occured when updating the username : \(error.localizedDescription)")
                }
            })
            
            
            guard let currentUsername = authResult?.user.uid else { return }
            Firestore.firestore().collection(USERS_REF).document(currentUsername).setData([
                USERNAME: username,
                EMAİL: email,
                USER_CREATION_DATE: FieldValue.serverTimestamp()
            ], completion: {(error) in
                if let error = error {
                    debugPrint("Error occured when adding the user : \(error.localizedDescription)")
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            })
        
            
        }
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}
