//
//  SendMessageViewController.swift
//  firebase-chat-app
//
//  Created by Ali Şengür on 21.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit

class SendMessageViewController: UIViewController {

    var titleText: String? {
        didSet{
            self.navigationItem.title = titleText
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

}
